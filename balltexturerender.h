#ifndef BALLTEXTURERENDER_H
#define BALLTEXTURERENDER_H

#include <QOpenGLExtraFunctions>
#include <QOpenGLTexture>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QImage>
#define PI 3.14159265f

class BallTextureRender
{
public:
    BallTextureRender() = default;
    ~BallTextureRender();
    void initsize(float radius, QImage &img);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix);

private:
    QOpenGLShaderProgram program_;
    QOpenGLTexture *texture_{nullptr};
    QOpenGLBuffer vbo_;
    float r_ = 0.0f;
    QVector<GLfloat> vertexPoins_;
    QVector<GLfloat> points_;

    QVector<GLfloat> generateTexCoor(int bw, int bh);
};

#endif // BALLTEXTURERENDER_H
