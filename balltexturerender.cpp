#include "balltexturerender.h"

BallTextureRender::~BallTextureRender()
{
    if(texture_){
        texture_->destroy();
        delete texture_;
    }
}

void BallTextureRender::initsize(float radius, QImage &img)
{
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vert");
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.frag");
    program_.link();

    r_ = radius;//弧度 = 角度 * PI / 180
    float ANGLE_SPAN=18.0f;
    float angleV=90;
    for(float vAngle=angleV;vAngle>0;vAngle=vAngle-ANGLE_SPAN)//垂直方向angleSpan度一份
    {
        for(float hAngle=360;hAngle>0;hAngle=hAngle-ANGLE_SPAN)//水平方向angleSpan度一份
        {
            //纵向横向各到一个角度后计算对应的此点在球面上的四边形顶点坐标
            //并构建两个组成四边形的三角形
            double xozLength=radius*::cos(vAngle * PI / 180);
            float x1=(float)(xozLength*::cos(hAngle * PI / 180));
            float z1=(float)(xozLength*::sin(hAngle * PI / 180));
            float y1=(float)(radius*::sin(vAngle * PI / 180));

            xozLength=radius*::cos((vAngle-ANGLE_SPAN) * PI / 180);
            float x2=(float)(xozLength*::cos(hAngle * PI / 180));
            float z2=(float)(xozLength*::sin(hAngle * PI / 180));
            float y2=(float)(radius*::sin((vAngle-ANGLE_SPAN) * PI / 180));

            xozLength=radius*::cos((vAngle-ANGLE_SPAN) * PI / 180);
            float x3=(float)(xozLength*::cos((hAngle-ANGLE_SPAN) * PI / 180));
            float z3=(float)(xozLength*::sin((hAngle-ANGLE_SPAN) * PI / 180));
            float y3=(float)(radius*::sin((vAngle-ANGLE_SPAN) * PI / 180));

            xozLength=radius*::cos(vAngle * PI / 180);
            float x4=(float)(xozLength*::cos((hAngle-ANGLE_SPAN) * PI / 180));
            float z4=(float)(xozLength*::sin((hAngle-ANGLE_SPAN) * PI / 180));
            float y4=(float)(radius*::sin(vAngle * PI / 180));

            //构建第一三角形
            vertexPoins_ << x1 << y1 << z1
                    << x4 << y4 << z4
                    << x2 << y2 << z2;

            //构建第二三角形
            vertexPoins_ << x2 << y2 << z2
                    << x4 << y4 << z4
                    << x3 << y3 << z3;
        }
    }

    points_ << vertexPoins_ << generateTexCoor(static_cast<int>(360/ANGLE_SPAN),static_cast<int>(angleV/ANGLE_SPAN));

    texture_ = new QOpenGLTexture(img);
//    texture_->setMinMagFilters(QOpenGLTexture::NearestMipMapLinear,QOpenGLTexture::NearestMipMapLinear);
//    texture_->setWrapMode(QOpenGLTexture::ClampToEdge);
    vbo_.create();
    vbo_.bind();
    vbo_.allocate(points_.constData(),points_.count() * sizeof GLfloat);
}

void BallTextureRender::render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix)
{
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_CULL_FACE);
    program_.bind();
    vbo_.bind();
    f->glActiveTexture(GL_TEXTURE0 + 0);
    program_.setUniformValue("uPMatrix",pMatrix);
    program_.setUniformValue("uVMatrix",vMatrix);
    program_.setUniformValue("uMMatrix",mMatrix);
    program_.setUniformValue("uR",r_);
    program_.setUniformValue("sTexture",0);
    program_.enableAttributeArray(0);
    program_.enableAttributeArray(1);

    texture_->bind(0);
    program_.setAttributeBuffer(0,GL_FLOAT,0,3,3 * sizeof GLfloat);
    program_.setAttributeBuffer(1,GL_FLOAT,vertexPoins_.count()*sizeof(GLfloat),2,2 * sizeof GLfloat);
    f->glDrawArrays(GL_TRIANGLES,0,vertexPoins_.count() / 3);

    program_.disableAttributeArray(0);
    program_.disableAttributeArray(1);
    texture_->release();
    vbo_.release();
    program_.release();
    f->glDisable(GL_DEPTH_TEST);
        f->glDisable(GL_CULL_FACE);
}

QVector<GLfloat> BallTextureRender::generateTexCoor(int bw, int bh)
{
    QVector<GLfloat> texVec;
    float sizew=1.0f/bw;//列数
    float sizeh=1.0f/bh;//行数
    int c=0;
    for(int i=0;i<bh;i++)
    {
        for(int j=0;j<bw;j++)
        {
            //每行列一个矩形，由两个三角形构成，共六个点，12个纹理坐标
            float s=j*sizew;
            float t=i*sizeh;

            texVec << s << t
                   << s+sizew << t
                   << s << t+sizeh
                   << s << t+sizeh
                   << s+sizew << t
                   <<s+sizew << t+sizeh;
        }
    }
    return texVec;
}
