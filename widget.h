#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include <QTimer>
#include "balltexturerender.h"
class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w,int h) override;

private:
    BallTextureRender render_;
    QVector3D lightLocation_,camera_;
    QMatrix4x4 pMatrix;
    QTimer tm_;
    float angle_ = 0;

private slots:
    void slotTimeout();
};

#endif // WIDGET_H
